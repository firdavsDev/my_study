## Sequence of executing

1. FROM
2. WHERE
3. GROUP BY
4. HAVING
5. SELECT
6. ORDER BY	

#
## Create table (CREATE TABLE)
```sql
CREATE TABLE table_name(field_name field_type, field_name field_type...)
```
#### Example:
```sql
CREATE TABLE book(
    book_id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(30),
    author VARCHAR(30),
    price DECIMAL(8, 2),
    amount INT
);
```
```sql
CREATE TABLE book (
    book_id INT PRIMARY KEY AUTO_INCREMENT, 
    title VARCHAR(50), 
    author_id INT NOT NULL, 
    price DECIMAL(8,2), 
    amount INT, 
    FOREIGN KEY (author_id)  REFERENCES author (author_id) 
);
```
#
## Insent data into table (INSERT)
```sql
INSERT INTO таблица(поле1, поле2) 
VALUES (значение1, значение2);
```
#### Example:
```sql
INSERT book (title, author, price, amount)
     VALUES ('Белая гвардия', 'Булгаков М.А.', 540.50, 5);
```
```sql  
INSERT book (title, author, price, amount)
     VALUES ('Идиот', 'Достоевский Ф.М.', 460.00, 10);
```
```sql
INSERT book (title, author, price, amount)
     VALUES ('Братья Карамазовы', 'Достоевский Ф.М.', 799.01, 2);
```
```sql
INSERT INTO 
    supply(title, author, price, amount)
    VALUES
    ('Лирика', 'Пастернак Б.Л.', 518.99, 2),
    ('Черный человек', 'Есенин С.А.', 570.20, 6),
    ('Белая гвардия', 'Булгаков М.А.', 540.50, 7),
    ('Идиот', 'Достоевский Ф.М.', 360.80, 3);
```
```sql
INSERT INTO book (title, author, price, amount);
```
```sql 
SELECT title, author, price, amount 
FROM supply
WHERE title NOT IN (
        SELECT title 
        FROM book
      );
```
#   
## Comment in query
```sql
/* Comment text */
```
#	
## Selecting data from table (SELECT)
```sql
SELECT field_name FROM table_name;
```
#### Example:
```sql
SELECT * FROM book; /* Selecting all data from table book */
```
```sql
SELECT title, amount FROM book; /* Selecting title and amount data from table book */
```	
#    	
## Selecting data with alias from table (AS)
```sql
SELECT field_name AS new_name FROM table_name;
```
#### Example:
```sql
SELECT title AS Название, author AS Автор FROM book;
```
#
## Selecting data with annotate (AS)
```sql
SELECT any calculation with field_names AS new_field FROM table_name;
```
#### Example:
```sql
SELECT title, price, amount, price * amount AS total FROM book;
```
```sql
SELECT title, 
    price, 
    ROUND((price*18/100)/(1+18/100),2) AS tax, 
    ROUND(price/(1+18/100),2) AS price_tax 
FROM book;
```
```sql
SELECT title, author, amount, ROUND(price * 0.70, 2) AS new_price FROM book;
```
#
## Selecting data with annotate with conditions (IF, CASE WHEN THEN END)
#### Example:
```sql
SELECT author, title, 
ROUND(CASE author
    WHEN "Булгаков М.А." THEN price * 1.1
    WHEN "Есенин С.А." THEN price * 1.05
    ELSE price END, 2) AS new_price
FROM book;
```
```sql
SELECT author, title, 
        ROUND(price * 
              IF(author="Булгаков М.А.", 1.1, IF(author="Есенин С.А.", 1.05, 1)
        ), 2) AS new_price
FROM book;
```
#	
## Selecting data with conditions (WHERE, BETWEEN, IN)
#### Example:
```sql
SELECT title, price FROM book WHERE price < 600;
SELECT title, author, price * amount AS total
FROM book
WHERE price * amount > 4000;
```
```sql
SELECT title, author, price 
FROM book
WHERE (author = 'Булгаков М.А.' OR author = 'Есенин С.А.') AND price > 600;
```
```sql
SELECT title, amount 
FROM book
WHERE amount BETWEEN 5 AND 14;
```
```sql
SELECT title, price 
FROM book
WHERE author IN ('Булгаков М.А.', 'Достоевский Ф.М.');
```
#
## Ordering query (ORDER BY, ASC, DESC)
#### Example:
```sql
SELECT title, author, price
FROM book
ORDER BY 1;
```
```sql
SELECT author, title, amount AS Количество
FROM book
WHERE price < 750
ORDER BY author, Количество DESC;
```
```sql
SELECT author, title, amount AS Количество
FROM book
WHERE price < 750
ORDER BY 1, 3 DESC;
```
#	
## Selecting string data with conditions (LIKE, NOT LIKE)
#### Example:
```sql
SELECT title FROM book 
WHERE title LIKE "______%";
```
```sql
SELECT title FROM book 
WHERE title LIKE "_____";
```
```sql
SELECT title FROM book 
WHERE title NOT LIKE "% %";
```
```sql
SELECT title, author
FROM book
WHERE title LIKE '_% _%' AND author LIKE '%С.%';
```
#
## Selecting unique items of columns (DISTINCT)
#### Example:
```sql
SELECT DISTINCT author
FROM book;
SELECT  author
FROM book
GROUP BY author;
```
#
## Selecting with group by (COUNT, SUM, MIN, MAX, AVG, GROUP BY)
#### Example:
```sql
SELECT author, sum(amount), count(amount)
FROM book
GROUP BY author;
```
```sql
SELECT author, SUM(amount)
FROM book
GROUP BY author;
```
```sql
SELECT author, COUNT(author), COUNT(amount), COUNT(*)
FROM book
GROUP BY author;
```
```sql
SELECT author as Автор,
COUNT(title) as Различных_книг,
SUM(amount) as Количество_экземпляров 
FROM book GROUP BY author;
```
```sql
SELECT author, 
MIN(price) as 'Минимальная_цена', 
MAX(price) as 'Максимальная_цена', 
AVG(price) as 'Средняя_цена'
FROM book GROUP BY author;
```
```sql
SELECT author, SUM(price*amount) as 'Стоимость', 
ROUND(SUM(price*amount) * 0.18 / (1+0.18), 2) as 'НДС',
ROUND(SUM(price*amount)-(SUM(price*amount) * 0.18 / (1+0.18)), 2) as 'Стоимость_без_НДС'
FROM book GROUP BY author;
```
#
## Selecting with group functions without GROUP BY
#### Example:
```sql
SELECT SUM(amount) AS Количество
FROM book;
```
```sql
SELECT MIN(price) AS 'Минимальная_цена',
MAX(price) AS 'Максимальная_цена',
ROUND(AVG(DISTINCT price), 2) as 'Средняя_цена'
FROM book;
```
```sql
SELECT ROUND(AVG(price), 2) AS 'Средняя_цена', 
SUM(price*amount) AS 'Стоимость' FROM book 
WHERE amount BETWEEN 5 AND 14;
```
#
## Selecting with group functions by Having (HAVING)
#### Example:
```sql
SELECT author,
    MIN(price) AS Минимальная_цена,
    MAX(price) AS Максимальная_цена
FROM book
WHERE author <> 'Есенин С.А.'
GROUP BY author
HAVING SUM(amount) > 10;
```
#
## Subqueries
#### Example:
```sql
SELECT title, author, price, amount
FROM book
WHERE price = (
         SELECT MIN(price) 
         FROM book
      );
```
```sql
SELECT author, title, price FROM book 
WHERE price <= (SELECT AVG(price) FROM book)
ORDER BY price DESC;
```
```sql
SELECT author, title, price FROM book 
WHERE price <= ((SELECT MIN(price) FROM book) + 150)
ORDER BY price;
```
```sql
SELECT author, title, amount
FROM book
WHERE amount IN (SELECT amount FROM book GROUP BY amount HAVING COUNT(amount) = 1);
```
#
## Subqueries with ANY and ALL (ANY, ALL)
#### Example:
```sql
SELECT title, author, amount, price
FROM book
WHERE amount < ALL (
        SELECT AVG(amount) 
        FROM book 
        GROUP BY author 
      );
``` 
#      
## Update items in tables
#### Example:
```sql
UPDATE book
SET price = 0.9 * price
WHERE amount BETWEEN 5 AND 10;
```
```sql
UPDATE book, supply
SET book.amount = book.amount + supply.amount,
    book.price = (book.price + supply.price) / 2
WHERE book.title = supply.title AND book.author = supply.author;
```
#
## Delete items from tables
```sql
DELETE FROM table_name;
```
#### Example:
```sql
DELETE FROM supply; /* Deleting all items from supply */
```
```sql
DELETE FROM supply 
WHERE title IN (
        SELECT title 
        FROM book
      );
```	
#	
## Operator Limit (LIMIT)
#### Example:
```sql
SELECT *
FROM trip
ORDER BY  date_first
LIMIT 1;
```
#
## Date functions (MONTH, DAY, YEAR, DATEDIFF, MONTHNAME)
#### Example:
```sql
SELECT name, city, date_first, date_last FROM trip
WHERE MONTH(date_first) = MONTH(date_last) ORDER BY city, name;
```
```sql
SELECT name, city, date_first, date_last FROM trip
WHERE DATEDIFF(date_last, date_first) = (SELECT MIN(DATEDIFF(date_last, date_first)) FROM trip);
```
```sql
SELECT buy.buy_id, 
       DATEDIFF(date_step_end, date_step_beg) AS Количество_дней,
       IF(DATEDIFF(date_step_end, date_step_beg) > days_delivery, DATEDIFF(date_step_end, date_step_beg) - days_delivery, 0) AS Опоздание
FROM buy INNER JOIN buy_step ON buy.buy_id = buy_step.buy_id
         INNER JOIN client ON buy.client_id = client.client_id
         INNER JOIN city ON city.city_id = client.city_id
         INNER JOIN step ON buy_step.step_id = step.step_id
WHERE name_step LIKE 'Транспортировка' AND date_step_end IS NOT NULL
```
#
## Alias for tables (AS)
#### Example:
```sql
SELECT  f.name, f.number_plate, f.violation, 
   if(
    f.sum_fine = tv.sum_fine, "Стандартная сумма штрафа", 
    if(
      f.sum_fine < tv.sum_fine, "Уменьшенная сумма штрафа", "Увеличенная сумма штрафа"
    )
  ) AS description               
FROM  fine f, traffic_violation tv
WHERE tv.violation = f.violation and f.sum_fine IS NOT Null;
```
```sql
UPDATE fine AS f, traffic_violation AS t
SET f.sum_fine = t.sum_fine WHERE f.violation = t.violation AND f.sum_fine IS NULL;
```
#
## On delete (ON DELETE)
```
CASCADE: автоматически удаляет строки из зависимой таблицы при удалении  связанных строк в главной таблице.
SET NULL: при удалении  связанной строки из главной таблицы устанавливает для столбца внешнего ключа значение NULL. (В этом случае столбец внешнего ключа должен поддерживать установку NULL).
SET DEFAULT похоже на SET NULL за тем исключением, что значение  внешнего ключа устанавливается не в NULL, а в значение по умолчанию для данного столбца.
RESTRICT: отклоняет удаление строк в главной таблице при наличии связанных строк в зависимой таблице.
```
#
## Joins (INNER JOIN, LEFT JOIN)
#### Example:
```sql
SELECT title, name_genre, price FROM book
INNER JOIN genre ON book.genre_id = genre.genre_id
WHERE amount > 8 ORDER BY price DESC;
```
```sql
SELECT name_author, title 
FROM author LEFT JOIN book
     ON author.author_id = book.author_id
ORDER BY name_author;
```
```sql
SELECT name_city, name_author, (DATE_ADD('2020-01-01', INTERVAL FLOOR(RAND() * 365) DAY)) as Дата
FROM author, city ORDER BY name_city, Дата Desc;
```
```sql
SELECT name_author, SUM(amount) AS Количество
FROM author AS b LEFT JOIN book AS a ON b.author_id = a.author_id
GROUP BY b.author_id HAVING Количество < 10 OR COUNT(title) = 0
ORDER BY Количество;
```
```sql
SELECT name_author FROM book INNER JOIN author on book.author_id = author.author_id 
GROUP BY book.author_id
HAVING COUNT(DISTINCT genre_id) = 1 ORDER BY name_author; -- COUNT DISTINCT == count only unique values.
```
#
## Update multiple tables with joins
#### Example:
```sql
UPDATE book 
     INNER JOIN author 
         ON author.author_id = book.author_id
     INNER JOIN supply 
         ON book.title = supply.title 
             and supply.author = author.name_author
SET book.amount = book.amount + supply.amount,
    supply.amount = 0   
WHERE book.price = supply.price;
```
```sql
UPDATE book
INNER JOIN author ON book.author_id = author.author_id
INNER JOIN supply ON book.title = supply.title
                  AND supply.author = author.name_author
                  AND supply.price <> book.price
SET book.price = (book.price * book.amount + supply.price * supply.amount) / (book.amount + supply.amount),
    book.amount = book.amount + supply.amount,
    supply.amount = 0;
    
SELECT * FROM book;
SELECT * FROM supply;
```
#
## Insert data with subquery
#### Example:
```sql
INSERT INTO author (name_author)
SELECT supply.author
FROM author 
RIGHT JOIN supply 
    ON author.name_author = supply.author
WHERE name_author IS Null;
```
#
## Delete item using joins
#### Example:
```sql
DELETE FROM author
USING 
    author 
    INNER JOIN book ON author.author_id = book.author_id
WHERE book.amount < 3;

SELECT * FROM author;

SELECT * FROM book;
```
