## Docker Image
* Image is a template for creating an environment of your Choice
* Image contains everyting need to run App

#
## Container
* Container is running instance of an Image

#
## Dockerfile
* File for building own images
* Dockerfile is name of file
* .dockerignore is file for ignoring files or folders when copying them from local to container
