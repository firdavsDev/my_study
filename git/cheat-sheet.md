## Caching credentials for push or pull
```bash
git config --global credential.helper cache
```
#### Cache for 1 hour:
```bash
git config --global credential.helper "cache --timeout=3600"
```
#
## Remote origin

#### Check remote origins:
```bash
git remote -v
```
#### Change remote url:
```bash
git remote set-url <new_url>
```

#
## Log commits
```bash
git log
```
#### Show only first line of commits
```bash
git log --oneline
```

#
## Branches
#### Local branches
```bash
git branch
```
#### Origin branches
```bash
git branch -r
```
#### All branches
```bash
git branch -a
```
#### Create new branch
```bash
git branch <new_branch_name>
```
#### Switch branch
```bash
git checkout <branch_name>
```
#### Switch to previous branch
```bash
git checkout -
```
#### Create and switch to new branch
```bash
git checkout -b <new_branch_name>
```
#### Delete branch
```bash
git branch -d <name_branch_name>
```
#### Merging branch to current branch
```bash
git merge <branch_name>
```
#### Change branch name
```bash
git branch -m <current_name> <new_name>
```
#### 

#
## Rebase
```bash
git pull -r origin main
```
```bash
git rebase--continue
```

#
## Push
#### Force push
```bash
git push -f
```