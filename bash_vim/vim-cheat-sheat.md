#### Set number
```
:set number or :set nu
```

#### Syntax on/off
```
:syntax off/on
```

#### Navigating around
```
j - down
k - up
h - left
l - right
```
```
w - forward by word
b - backward by word
```
```
0 - to beginning of line
$ - to end of line
^ - to beginning of line after spaces
```
```
gg or [[ - up to first line
G or ]] - down to end
```
```
{ - Up by sentence
} - Down by sentence
```
```
<number>gg - go to specific line
```
```
8gg - go to 8 - line
```

#### Undo in command mode
```
u
```

#### Redo in command mode
```
ctrl + r
```

#### Switch to insert mode
```
i - for writing before current character
a - for writing after current character
O - for writing before current line
o - for writing after current line
shift + a - for writing to the end of current line
```

#### Delete and paste
```
dw - delete word to forward
db - delete word to backward
dd - delete current line
<number>dd - delete lines <number> time
dG - delete everything from this line to the end
p - paste
```

#### Search
```
/<some_string> + Enter - find some_string
n - next
N - previous
, + space - remove highlights
```
